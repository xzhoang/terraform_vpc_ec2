env           = "test"
region        = "ap-east-1"
vpc_cidr      = "10.20.0.0/16"
subnet_cidrs  = ["10.20.1.0/24"]
instance_type = "t3.micro"
volume_size   = "10"
